#ifndef PERSONA_H
#define PERSONA_H
#include <string>
#include "CajaFuerte.h"

class Persona
{
    public:
        Persona();
        Persona (std::string, std::string, float, CajaFuerte*); // constructor parametrizado
        virtual ~Persona();
        std::string comoTeLlamas();
        std::string dondeVivis();
        void mudarse (std::string);
        float dineroTotal (); // devuelve la cantidad total de dinero de la persona
        void cobrar (float); // ahorra el 40% de lo cobrado
        bool puedeComprarAlgoQueCuesta (float); // determina si puede comprar algo
        void reasignarCajaFuerte (CajaFuerte*);

    protected:

    private:
        std::string ciudad;
        std::string nombre;
        CajaFuerte* miCajaFuerte;
        float efectivo;
};

#endif // PERSONA_H
